import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';
import ImagePicker from 'react-native-image-picker';

import { connect } from 'react-redux';

class Avatar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            avatar: require('../images/ic_tag_faces.png')
        }
    }

    _avatarClicked = () => {
        ImagePicker.showImagePicker({}, (response) => {
            if(response.didCancel){
                console.log('Operation canceled');
            } else if(response.error) {
                console.log('erros: ', response.error);
            } else {
                console.log("uri",response.uri);
                let requireSource = {uri: response.uri};
                
                const action = {type: 'SET_AVATAR', value: requireSource };
                this.props.dispatch(action);

            }
        })

    }

    render() {
       // console.log("AVATAR",this.props.avatar);
        return (
            <TouchableOpacity
                onPress={this._avatarClicked}
                style={styles.touchableopacity}
            >
                <Image 
                    source={this.props.avatar}
                    style={styles.avatar}
                    
                />
            </TouchableOpacity>
            
        )
    }
}

const styles = StyleSheet.create({
    touchableopacity: {
        margin: 5,
        width: 100, // Pensez bien à définir une largeur ici, sinon toute la largeur de l'écran sera cliquable
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
      width: 100,
      height: 100,
      borderRadius: 50,
      borderColor: '#9B9B9B',
      borderWidth: 2
    }
});

const mapStateToProps = state => {
    return {
        avatar: state.setAvatar.avatar
    }
}

export default connect(mapStateToProps)(Avatar);
