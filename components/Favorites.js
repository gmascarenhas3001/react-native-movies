import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native';
import FilmList from './FilmList';
import { connect } from 'react-redux';
import Avatar from './Avatar';


class Favorites extends Component {
    render() {
        return (
            <View style={styles.mani_container}>
                <View style={styles.avatar}>
                 <Avatar />
                </View>
                <FilmList
                    films={this.props.favoritesFilms}
                    navigation={this.props.navigation}
                    favoriteList={true}
                    
                 />
            </View>
           
        )
    }
}

const styles = StyleSheet.create({
    mani_container: {
        flex: 1
    },
    avatar: {
        alignItems: 'center'
    }
})

const matStateToProps = (state) => {
    
    return {
        favoritesFilms: state.toggleFavorite.favoritesFilms
    }
}

export default connect(matStateToProps)(Favorites);
