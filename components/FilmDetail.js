import React from 'react'
import { Share, StyleSheet, View, Text, ActivityIndicator, ScrollView, Image, TouchableOpacity, Platform, Button } from 'react-native'
import { getFilmdetailFromApi, getImageFromApi } from '../api/TMDBApi';
import moment from 'moment';
import numeral from 'numeral';
import EnlargeShrink from '../animatios/EnlargeShrink';

import { connect } from 'react-redux';



class FilmDetail extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    if(params.film !== undefined && Platform.OS === 'ios') {
      return {
        headerRight: <TouchableOpacity
        style={styles.share_touchable_headerrightbutton}
        onPress={() => params.shareFilm()}
        >
          <Image 
          style={style.share_image}
          source={require('../images/ic_share.ios.png')}
          />
        </TouchableOpacity>
              
      }
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      film: undefined,
      isLoading: true,
      filmVu: undefined
    }
  }

 /*  componentDidUpdate() {
    console.log( "componentDidUpdates", this.props.favoritesFilms);
  } */

  componentDidMount() {
    const favoriteFilmIndex = this.props.favoritesFilms.findIndex(item => item.id === this.props.navigation.state.params.idFilm)
    if (favoriteFilmIndex !== -1) { 
      this.setState({
        film: this.props.favoritesFilms[favoriteFilmIndex]
      }, () => { this._updateNavigationParams() })
      return
    }
    
    this.setState({isLoading: true});

    getFilmdetailFromApi(this.props.navigation.state.params.idFilm)
    .then(data => {
      this.setState({
        film: data,
        isLoading: false
      }, () => { this._updateNavigationParams() })
    })
    console.log("isLoading");
  }

  _toggleFavorites() {
    const action = {type: 'TOGGLE_FAVORITE', value: this.state.film};
    this.props.dispatch(action);
  }

  _displayfavoriteImage = () => {
    let imageSource = require('../images/non_favorite.png');
    let shouldEnlarge = false;
      if(this.props.favoritesFilms.findIndex(item => item.id === this.state.film.id) !== -1) {
        imageSource = require('../images/favorite.png');
        shouldEnlarge = true;
      }
      return (
      <EnlargeShrink shouldEnlarge={shouldEnlarge}>
        <Image
          source={imageSource}
          style={styles.favorite_image}
        />
      </EnlargeShrink>
      
      )
      
  }


  _displayFilm = () => {
    if(this.state.film != undefined) {
      return (
      <ScrollView style={styles.ScrollView_container}>
        <Image
          style={styles.image}
          source={{uri: getImageFromApi(this.state.film.backdrop_path)}}
        />
        <Text style={ styles.title_text }>{this.state.film.title}</Text>
        <TouchableOpacity 
          onPress={() => this._toggleFavorites()}
          style={styles.favorite_container}
        >{this._displayfavoriteImage()}</TouchableOpacity>
        <Text style={ styles.description_text }>{this.state.film.overview}</Text>
        <Text style={ styles.default_text }>Sorti le {moment(new Date(this.state.film.release_date)).format('DD/MM/YYYY')}</Text>
        <Text style={styles.default_text}>Note : {this.state.film.vote_average} / 10</Text>
        <Text style={styles.default_text}>Nombre de votes : {this.state.film.vote_count}</Text>
        <Text style={styles.default_text}>Budget : {numeral(this.state.film.budget).format('0,0[.]00 $')}</Text>
        <Text style={styles.default_text}>Genre(s) : {this.state.film.genres.map(function(genre){
              return genre.name;
            }).join(" / ")}</Text>
        <Text style={styles.default_text}>Companie(s) : {this.state.film.production_companies.map(function(company){
              return company.name;
            }).join(" / ")}
          </Text>
      </ScrollView>
      )
    }
  }

  _shareFilm = () => {
    const { film } = this.state;
    Share.share({ title: film.title, message: film.overview}) 
  }

  _displayFloatingActionButton = () => {
    const { film } = this.state;
    if( film !== undefined && Platform.OS === 'android') {
      return (
        <TouchableOpacity 
        style={styles.share_touchable_floatingactionbutton}
        onPress={() => this._shareFilm()}
        >
          <Image 
          style={styles.share_image}
          source={require('../images/ic_share.android.png')}
          />
        </TouchableOpacity>
      )
    }
  }


  _updateNavigationParams = () => {
    this.props.navigation.setParams({
      shareFilm: this._shareFilm,
      film: this.state.film
    })
  }

  _isMarqueVu = () => {
    const action = {type: 'ADD_FILMVU', value: this.state.film};
    this.props.dispatch(action);
  }

  render() {
     //console.log("stackNavigator==>",this.props.navigation.state.params.film);
    console.log("detail==>>",this.props.filmsVu);
    return (
      <View style={styles.main_container}>
        {this.state.isLoading && this.state.film === undefined ? 
             <View style={styles.loading_container}>
                <ActivityIndicator size="large" color="black"/>
             </View> : null
        }
        {this._displayFilm()}
        {this._displayFloatingActionButton()}
        <Button onPress={() => this._isMarqueVu()} title='Marquer comme vu'/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  ScrollView_container: {
    flex: 1
  },
  image: {
    flex: 1,
    width: 400,
    height: 280,
    margin: 5,
    backgroundColor: 'gray'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 35,
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
    color: '#000000',
    textAlign: 'center'
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666',
    margin: 5,
    marginBottom: 15
  },
  default_text: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5,
  },
  favorite_container: {
    alignItems: 'center'
  },
  favorite_image: {
    flex: 1,
    width: null,
    height: null
  },
  share_touchable_floatingactionbutton: {
    position: 'absolute',
    width: 60,
    height: 60,
    right: 30,
    bottom: 30,
    borderRadius: 30,
    backgroundColor: '#e91e63',
    justifyContent: 'center',
    alignItems: 'center'
  },
  share_image: {
    width: 30,
    height: 30
  },
  share_touchable_headerrightbutton: {
    marginRight: 8
  }
})

const mapStateToProps = (state) => {
  return {
    favoritesFilms: state.toggleFavorite.favoritesFilms,
    filmsVu: state.filmsVuView.filmsVu
  }
}


export default connect(mapStateToProps)(FilmDetail);