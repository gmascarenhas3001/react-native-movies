import React from 'react';
import { StyleSheet, FlatList, Text, View } from 'react-native';
import FilmItem from './FilmItem';
import { connect } from 'react-redux';


class FilmList extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            films: []
        }
    }


    _displayDetailForFilm = (idFilm) => {
        console.log("Display film " + idFilm);
        this.props.navigation.navigate("FilmDetail", {idFilm: idFilm})
    }

    render() {
       // console.log("testList",this.props.films);
        return(
            <View>
                 <FlatList
                        data={this.props.films}
                        extraData={this.props.favoritesFilms}
                        keyExtractor={( item ) => item.id.toString()}
                        renderItem={({ item }) => 
                    <FilmItem 
                        film={item} 
                        displayDetailForFilm={this._displayDetailForFilm}
                        isFilmFavorite={(this.props.favoritesFilms.findIndex(film => film.id === item.id) !== -1) ? true : false}
                        />}
                        onEndReachedThreshold={0.5}
                        onEndReached={() => { 
                            if(this.props.favoritesFilms && this.props.page < this.props.totalPages) {
                                this.props.loadFilm()
                            }}
                            }
                    />
            </View>
        )
    }
}

const mapStateToProps = (state) => {
   return {
       favoritesFilms: state.toggleFavorite.favoritesFilms
    }
}

export default connect(mapStateToProps)(FilmList);