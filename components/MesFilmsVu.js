import React, { Component } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import FilmVu from './filmVu';

class MesFilmsVu extends Component {


    _displayFilmForDetail = (idFilm) => {
        this.props.navigation.navigate('FilmDetail', { idFilm: idFilm})
    }


    render() {
        console.log("filmVU", this.props.filmsVu);
        return (
            <View>
                <FlatList 
                    data={this.props.filmsVu}
                    extraData={this.props.filmsVu}
                    keyExtractor={( item ) => item.id.toString()}
                    renderItem={({ item }) => (
                    <FilmVu
                        films={item}
                        displayFilmForDetail={this._displayFilmForDetail}
                    />
                    )
                    
                    }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    list: {
      flex: 1
    }
  })

const mapStateToProps = (state) => {
    return {
        filmsVu: state.filmsVuView.filmsVu
    }
}

export default connect(mapStateToProps)(MesFilmsVu);
