import React, { Component } from 'react';
import { Image, View, StyleSheet, ActivityIndicator } from 'react-native';
import FilmList from './FilmList';

import { getFilmNews } from '../api/TMDBApi';

import { connect } from 'react-redux';


class News extends Component {

    constructor(props) {
        super(props)
        this.state = {
            avatar: require('../images/ic_tag_faces.png'),
            filmsNews: [],
            isLoading: false
        }
        this.page = 0;
        this.totalPages = 0;
    }

    componentDidMount() {
        this._isLoadingFilmNews();
    }

    _isLoadingFilmNews = () => {
        this.setState({isLoading: true});
        getFilmNews(this.page+1)
        .then(data => {
           // console.log("data.results", data.results);
            this.page = data.page;
            this.totalPages = data.total_pages;
            this.setState({
               filmsNews: [ ...this.state.filmsNews, ...data.results ],
               isLoading: false
            });

        });

    }

    render() {
        //console.log("NEWS=>",this.state.filmsNews);
        return (
            <View style={styles.main_container}>
                <View style={{ alignItems: 'center', marginTop: 20}}>
                    <Image 
                        style={styles.avatar}
                        source={this.props.avatar}
                    />
                    
                </View>

             { this.isLoading ? 
                <View>
                    <ActivityIndicator style={styles.loading_container} size='large' color='black' />
                </View>
                :
                <FilmList 
                    films={this.state.filmsNews}
                    navigation={this.props.navigation}
                    page={this.page}
                    totalPages={this.totalPages}
                    loadFilm={this._isLoadingFilmNews}
             /> }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderColor: '#9B9B9B',
        borderWidth: 2
    },
    loading_container: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    }
})

const mapStateToProps = state => {
    return {
        avatar: state.setAvatar.avatar
    }
}

export default connect(mapStateToProps)(News);
