import React from 'react';
import { View, TextInput, Button, StyleSheet, ActivityIndicator } from 'react-native';

import FilmList from './FilmList';
import { getFilmsFromApiWithSearchedText } from '../api/TMDBApi';

import { connect } from 'react-redux';

class Search extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            films: [],
            isLoading: false
        }
        this.searchedText = "";
        this.page = 0;
        this.totalPages = 0;
    }

    _loadFilm = () => {
        this.setState({isLoading: true});
        if(this.searchedText.length > 0){
            getFilmsFromApiWithSearchedText(this.searchedText, this.page+1)
            .then(data => {
                this.page = data.page;
                this.totalPages = data.total_pages;
                console.log(data.total_pages)
                this.setState({
                films: [...this.state.films, ...data.results],
                isLoading: false
            })
            }
            );
        }
    }

    _searchTextInputChanged(text) {
        this.searchedText = text;
    }

   seachFilm = () => {
       this.page = 0;
       this.total_pages = 0;
       this.setState({films: [] },
        () => {
            console.log("Page:" + this.page + "/Total_pages:" + this.totalPages + "/Films" + this.state.films.length);
            this._loadFilm();
        }
        );
    }

    displayFilmForDetail = (idFilm, filmState ) => {
        
        this.props.navigation.navigate("FilmDetail", {idFilm: idFilm, filmState: filmState });
    }

    render() {
        //console.log("TESTtttt", this.props.navigation);
        return (
            <View style={styles.container}>
                <TextInput onSubmitEditing={() => this.seachFilm()} onChangeText={(text) => this._searchTextInputChanged(text)} style={styles.input} placeholder="Titre du film"/>
                <Button style={styles.button} title="Rechercher" onPress={() => this.seachFilm()}/>
                <FilmList
                    films={this.state.films} // C'est bien le component Search qui récupère les films depuis l'API et on les transmet ici pour que le component FilmList les affiche
                    navigation={this.props.navigation} // Ici on transmet les informations de navigation pour permettre au component FilmList de naviguer vers le détail d'un film
                    loadFilm={this._loadFilm} // _loadFilm charge les films suivants, ça concerne l'API, le component FilmList va juste appeler cette méthode quand l'utilisateur aura parcouru tous les films et c'est le component Search qui lui fournira les films suivants
                    page={this.page}
                    totalPages={this.totalPages} // les infos page et totalPages vont être utile, côté component FilmList, pour ne pas déclencher l'évènement pour charger plus de film si on a atteint la dernière page
                    />
                {this.state.isLoading ? 
                <View style={styles.loading_container}>
                    <ActivityIndicator size="large" color="black"/>
                </View> : null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    input: {
        marginLeft: 5,
        marginRight: 5,
        height: 50,
        borderColor: '#000000',
        borderWidth: 1, 
        paddingLeft: 5,
        marginBottom: 10     
    }, 
    button: {
        height: 50
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
      }
})

const mapStateToProps = (state) => {

   return {
        favoritesFilms: state.toggleFavorite.favoritesFilms
    }
}

export default connect(mapStateToProps)(Search);