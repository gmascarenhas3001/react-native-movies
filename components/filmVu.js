import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import moment from 'moment'

 class filmVu extends Component {

    constructor(props) {
        super(props)
        this.state = {
            displayDate: false
        }
    }

    _displayDate = () => {
        this.setState({displayDate: !this.state.displayDate})
    }

    _displayText() {
        if (this.state.displayDate) {
          return (
            <Text style={styles.text}>Sorti le {moment(new Date(this.props.films.release_date)).format('DD/MM/YYYY')}</Text>
          )
        }
        else {
          return (
            <Text style={styles.text}>{this.props.films.title}</Text>
          )
        }
      }

    render() {
        console.log("VU===>",this.props.films.release_date);
        return (
            <TouchableOpacity
                style={styles.main_container}
                onPress={() => this.props.displayFilmForDetail(this.props.films.id)}
                onLongPress={() => this._displayDate()}>
            
                <View style={styles.content_container}>
                    {this._displayText()}
                    
                </View>
           </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
      height: 100,
      flexDirection: 'row'
    },
    image: {
      height: 80,
      width: 80,
      margin: 10,
      borderRadius: 40 // Pour créer un rond parfait, il faut que borderRadius vale la moitié de la hauteur et la largeur de l'image (80 / 2 = 40)
    },
    content_container: {
      flex: 1,
      justifyContent: 'center',
      margin: 10
    },
    text: {
      fontSize: 20
    }
  })

export default filmVu;
