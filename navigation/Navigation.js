import React from 'react';
import { createAppContainer } from 'react-navigation';
import Search from '../components/Search';
import FilmDetail from '../components/FilmDetail';
import News from '../components/News';
import MesFilmsVu from '../components/MesFilmsVu';

import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Favorites from '../components/Favorites';
import { Image, StyleSheet } from 'react-native';

const SearchStackNavigator =  createStackNavigator({
    Search: {
        screen: Search,
        navigationOptions: {
            title: "Rechercher"
        }
    },
    FilmDetail: {
        screen: FilmDetail,
        navigationOptions: {
            title:"Film"
        }
    }
});

const FavoritesStackNavigator = createStackNavigator({
  Favorites: {
    screen: Favorites,
    navigationOptions: {
      title: 'Favoris'
    }
  },
  FilmDetail: {
    screen: FilmDetail
  }
});

const NewsStackNavigator = createStackNavigator({
  News: {
    screen: News,
    navigationOptions: {
      title: 'News'
    }
  },
  FilmDetail: {
    screen: FilmDetail
  }
});

const MesFilmsVuStackNavigator = createStackNavigator({
  News: {
    screen: MesFilmsVu,
    navigationOptions: {
      title: 'Mes films vu'
    }
  },
  FilmDetail: {
    screen: FilmDetail
  }
});


const MoviesTabNavigator = createBottomTabNavigator({

    Search: {
      screen: SearchStackNavigator,
      navigationOptions: {
        tabBarIcon: () => {
          return <Image
            source={require('../images/search.png')}
            style={styles.icon}/>
        }
      }
    },
    Favorites: {
      screen: FavoritesStackNavigator,
      navigationOptions: {
        tabBarIcon: () => {
          return <Image
            source={require('../images/favorite.png')}
            style={styles.icon}/>
        }
      }
    }, 
    News: {
      screen: NewsStackNavigator,
      navigationOptions: {
        tabBarIcon: () => {
          return <Image
            source={require('../images/ic_news.png')}
            style={styles.icon}/>
        }
      }
    },
    MesFilsVu: {
      screen: MesFilmsVuStackNavigator,
      navigationOptions: {
        tabBarIcon: () => {
          return <Image
            source={require('../images/filmsVu.png')}
            style={styles.icon}/>
        }
      }
    }

  },
  {
    tabBarOptions: {
        activeBackgroundColor: '#DDDDDD', // Couleur d'arrière-plan de l'onglet sélectionné
        inactiveBackgroundColor: '#FFFFFF',
        showLabel: false,
        showIcon: true
    }
  }
  
  )

  const styles = StyleSheet.create({
      icon: {
          width: 30,
          height: 30
      }
  }) 

export default createAppContainer(MoviesTabNavigator);