import { createStore, combineReducers } from 'redux';
import toggleFavorite from './reducers/favoriteReducer';
import setAvatar from './reducers/avatarReducer';
import filmsVuView from './reducers/filmsVuReducer';


import {  persistCombineReducers } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
//import { persistReducer, persistStore } from 'redux-persist';

const rootPersistConfig = {
  key: 'root',
  storage: AsyncStorage
}


export default createStore(persistCombineReducers(rootPersistConfig, {toggleFavorite, setAvatar, filmsVuView}))
