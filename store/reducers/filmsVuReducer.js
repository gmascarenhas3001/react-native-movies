const inicialState = {filmsVu: []};

function filmsVuView (state = inicialState, action ) {
    let nextState;
    switch(action.type) {
        case 'ADD_FILMVU':
            const filmsVuIndex = state.filmsVu.findIndex(item => item.id === action.value.id);
            if(filmsVuIndex !== -1) {
                nextState = {
                    ...state,
                    filmsVu: state.filmsVu.filter((item, index) => index !== filmsVuIndex)
                }
            } else {
                nextState = {
                    ...state,
                    filmsVu: [...state.filmsVu, action.value]
                }
            }
            return nextState;
        
        default: 
            return state;
    }
}

export default filmsVuView;